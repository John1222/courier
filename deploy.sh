#!/usr/bin/env bash

sudo apt-get update

# docker engine
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
sudo apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main'
sudo apt-get update
sudo apt-get install docker-engine -y

# docker compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# рабочая папка
sudo mkdir -p /var/www/html
sudo chmod 755 /var/www/html
cd /var/www/html

# git
sudo apt-get install git

# качаем репо
sudo git clone https://brocoder1@bitbucket.org/brocoder1/couriertest.git .

# создаем сетку
sudo docker network create external-net

# создаем volume
sudo docker volume create --name=pgdata

# подымаем контейнеры
sudo docker rm -f $(sudo docker ps -a -q)
sudo docker-compose up -d

# ставим композерные зависимости
sudo docker exec -it courier_web_1 composer install

# раздаем права
sudo docker exec -it courier_web_1 chmod 764 ./bin/console

# создаем БД
sudo docker exec -it courier_web_1 ./bin/console doctrine:database:create

# создаем таблицы
sudo docker exec -it courier_web_1 ./bin/console doctrine:schema:create

# заполняем таблицы тестовыми данными
sudo docker exec -it courier_web_1 ./bin/console doctrine:fixtures:load -q

# стартуем тесты. Все должно быть зеленым
sudo docker exec -it courier_web_1 ./vendor/bin/codecept run