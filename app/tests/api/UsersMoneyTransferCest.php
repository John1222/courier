<?php
namespace App\Tests;

use App\DataFixtures\AppFixtures;
use App\Entity\Transaction;
use App\Entity\User;

class UsersMoneyTransferCest
{
    /**
     * @var ApiTester
     */
    private $I;

    /**
     * @var int
     */
    private const TRANSFER_AMOUNT = 1000;
    /**
     * @var int
     */
    private $increaseSenderBalanceForAmount = self::TRANSFER_AMOUNT;
    /**
     * @var User
     */
    private $sender;
    /**
     * @var int
     */
    private $senderBalanceBefore;
    /**
     * @var User
     */
    private $recipient;
    /**
     * @var int
     */
    private $recipientBalanceBefore;

    public function _before( ApiTester $I )
    {
        $this->I = $I;
        $I->loadFixtures( AppFixtures::class );
    }

    public function _after( ApiTester $I )
    {
        $this->I->clearEntityManager();
    }

    public function testSenderBalanceDecreasing( ApiTester $I )
    {
        $this->transfer( self::TRANSFER_AMOUNT, $this->increaseSenderBalanceForAmount );
        $I->assertEquals( $this->senderBalanceBefore - self::TRANSFER_AMOUNT, $this->sender->getBalance() );
        $I->assertContains( 'Transfer completed', $I->grabResponse() );
    }

    public function testRecipientBalanceIncreasing( ApiTester $I )
    {
        $this->transfer( self::TRANSFER_AMOUNT, $this->increaseSenderBalanceForAmount );
        $I->assertEquals( $this->recipientBalanceBefore + self::TRANSFER_AMOUNT, $this->recipient->getBalance() );
        $I->assertContains( 'Transfer completed', $I->grabResponse() );
    }

    public function testTransactionRegistering( ApiTester $I )
    {
        $this->transfer( self::TRANSFER_AMOUNT, $this->increaseSenderBalanceForAmount );
        $I->canSeeInRepository(
            Transaction::class,
            [
                'sender' => $this->sender->getId(),
                'recipient' => $this->recipient->getId(),
                'amount' => self::TRANSFER_AMOUNT
            ]
        );
        $I->assertContains( 'Transfer completed', $I->grabResponse() );
    }

    public function testUserCannotTransferAmountExceededHisBalance( ApiTester $I )
    {
        $this->transfer(
            self::TRANSFER_AMOUNT,
            $this->increaseSenderBalanceForAmount - 1001
        );
        $I->cantSeeInRepository(
            Transaction::class,
            [
                'sender' => $this->sender->getId(),
                'recipient' => $this->recipient->getId(),
                'amount' => self::TRANSFER_AMOUNT
            ]
        );
        $I->assertContains( 'Sender has insufficient funds', $I->grabResponse() );
    }

    /**
     * @param int $amount
     * @param int $senderStartingBalance
     */
    private function transfer( $amount, $senderStartingBalance )
    {
        $this->sender = $this->I->grabEntityFromRepository( User::class, [ 'isClient' => 1 ] );
        $this->sender->increaseBalance( $senderStartingBalance );
        $this->I->persistEntity( $this->sender );
        $this->I->flushToDatabase();
        $this->senderBalanceBefore = $this->sender->getBalance();
        $this->recipient = $this->I->grabEntityFromRepository( User::class, [ 'isClient' => 0 ] );
        $this->recipientBalanceBefore = $this->recipient->getBalance();
        $this->I->sendGET(
            "/money_transfer/?sender={$this->sender->getId()}" .
            "&recipient={$this->recipient->getId()}" .
            "&amount=" . $amount
        );
    }
}