<?php
namespace App\Tests;

use App\DataFixtures\AppFixtures;
use App\Entity\Delivery;
use App\Entity\Transaction;
use App\Entity\User;

class DeliveryCest
{
    /**
     * @var ApiTester
     */
    private $I;
    
    public function _before( ApiTester $I )
    {
        $this->I = $I;
        $I->loadFixtures( AppFixtures::class );
    }
    
    public function _after( ApiTester $I )
    {
        $I->clearEntityManager();
    }

    public function testCourierRewardObtaining( ApiTester $I )
    {
        $this->deliveryExecute( 1 );
        $delivery = $this->grabDelivery( 1 );
        $I->assertEquals(
            $delivery->getCourierReward(),
            $delivery->getCourier()->getBalance()
        );
        $I->assertContains( 'Delivery completed', $I->grabResponse() );
    }
    
    public function testClientShouldHaveMoneyForCourierReward( ApiTester $I )
    {
        /** @var Delivery $delivery */
        $delivery = $this->grabDelivery( 1 );
        /** @var User $client */
        $client = $delivery->getOrder()->getClient();
        $client->decreaseBalance( $client->getBalance() );
        $this->deliveryExecute( 1 );
        $I->assertContains( 'Client have not enough funds for courier reward', $I->grabResponse() );
    }
    
    public function testDeliveryClosureAfterCompletion( ApiTester $I )
    {
        $this->deliveryExecute( 1 );
        $I->assertFalse( $this->grabDelivery( 1 )->getIsActive() );
        $I->assertContains( 'Delivery completed', $I->grabResponse() );
    }
    
    public function testCourierCannotExecuteClosedDelivery( ApiTester $I )
    {
        $this->deliveryExecute( 1 );
        $courierBalanceBefore = $this->grabDelivery( 1 )->getCourier()->getBalance();
        $this->deliveryExecute( 1 );
        $courierBalanceAfter = $this->grabDelivery( 1 )->getCourier()->getBalance();
        $I->assertEquals( $courierBalanceBefore, $courierBalanceAfter );
        $I->assertEquals( 'Delivery closed', $I->grabResponse() );
    }

    public function testShipmentLocationChanging( ApiTester $I )
    {
        $this->deliveryExecute( 1 );
        $delivery = $this->grabDelivery( 1 );
        $I->assertEquals(
            $delivery->getShipment()->getLocation(),
            $delivery->getLocation()
        );
        $I->assertContains( 'Delivery completed', $I->grabResponse() );
    }

    public function testCourierRewardTransactionRegistering( ApiTester $I )
    {
        $this->deliveryExecute( 1 );
        $delivery = $this->grabDelivery( 1 );
        $I->canSeeInRepository(
            Transaction::class,
            [
                'sender' => $delivery->getOrder()->getClient()->getId(),
                'recipient' => $delivery->getCourier(),
                'amount' => $delivery->getCourierReward()
            ]
        );
        $I->assertContains( 'Delivery completed', $I->grabResponse() );
    }

    public function testShouldDiffersShipmentAndDeliveryLocations( ApiTester $I )
    {
        $this->deliveryExecute( 2 );
        $delivery = $this->grabDelivery( 2 );
        $this->I->assertTrue( $delivery->getIsActive() );
        $I->assertContains( 'Shipment already in location', $I->grabResponse() );
    }

    /**
     * @param $id
     */
    private function deliveryExecute( $id )
    {
        $this->I->sendGET( "/delivery_execute/{$id}" );
    }

    /**
     * @param int $id
     * @return Delivery
     */
    private function grabDelivery( $id )
    {
        return $this->I->grabEntityFromRepository( Delivery::class, [ 'id' => $id ] );
    }
}
