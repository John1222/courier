<?php
namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="`transactions`")
 **/
class Transaction
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    protected $amount;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @var User
     */
    protected $sender;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @var User
     */
    protected $recipient;
    
    /**
     * @ORM\Column(type="datetime")
     * @var DateTime
     */
    protected $date;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @return User
     */
    public function getSender(): User
    {
        return $this->sender;
    }

    /**
     * @return User
     */
    public function getRecipient(): User
    {
        return $this->recipient;
    }

    /**
     * @return DateTime
     */
    public function getDate(): DateTime
    {
        return $this->date;
    }

    /**
     * @param User $recipient
     */
    public function setRecipient( User $recipient ): void
    {
        $this->recipient = $recipient;
    }

    /**
     * @param User $sender
     */
    public function setSender( User $sender ): void
    {
        $this->sender = $sender;
    }

    /**
     * @param int $amount
     */
    public function setAmount( int $amount ): void
    {
        $this->amount = $amount;
    }

    /**
     * @param DateTime $date
     */
    public function setDate( DateTime $date ): void
    {
        $this->date = $date;
    }
}