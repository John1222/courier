<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="`users`")
 **/
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @var int
     */
    protected $id;
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $name;
    /**
     * @ORM\Column(type="boolean", name="is_client")
     * @var boolean
     */
    protected $isClient = false;
    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    protected $balance = 0;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function getIsClient()
    {
        return $this->isClient;
    }

    /**
     * @param boolean $isClient
     */
    public function setClient( $isClient )
    {
        $this->isClient = $isClient;
    }

    /**
     * @return int
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param $name
     */
    public function setName( $name )
    {
        $this->name = $name;
    }

    /**
     * @param int $amount
     */
    public function increaseBalance( int $amount )
    {
        $this->balance += $amount;
    }

    /**
     * @param int $amount
     */
    public function decreaseBalance( int $amount )
    {
        $this->balance -= $amount;
    }
}