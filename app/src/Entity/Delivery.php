<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="`deliveries`")
 **/
class Delivery
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @var int
     */
    protected $id;
    /**
     * @ORM\Column(type="boolean", name="is_active")
     * @var boolean
     */
    protected $isActive = true;
    /**
     * @ORM\ManyToOne(targetEntity="Order")
     * @var Order
     */
    protected $order;
    /**
     * @ORM\ManyToOne(targetEntity="Shipment")
     * @var Shipment
     */
    protected $shipment;
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $location;
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @var User
     */
    protected $courier;
    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    protected $courierReward;

    /**
     * @param bool $isActive
     */
    public function setActive( bool $isActive )
    {
        $this->isActive = $isActive;
    }

    /**
     * @param Order $order
     */
    public function setOrder( Order $order )
    {
        $this->order = $order;
    }

    /**
     * @param Shipment $shipment
     */
    public function setShipment( Shipment $shipment )
    {
        $this->shipment = $shipment;
    }

    /**
     * @param string $location
     */
    public function setLocation( string $location )
    {
        $this->location = $location;
    }

    /**
     * @param int $courierReward
     */
    public function setCourierReward( int $courierReward )
    {
        $this->courierReward = $courierReward;
    }

    /**
     * TODO если успеем: разбить курьера и клиента на разные классы
     * @param User $courier
     */
    public function setCourier( User $courier )
    {
        $this->courier = $courier;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function getIsActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @return Shipment
     */
    public function getShipment(): Shipment
    {
        return $this->shipment;
    }

    /**
     * @return string
     */
    public function getLocation(): string
    {
        return $this->location;
    }

    /**
     * @return int
     */
    public function getCourierReward(): int
    {
        return $this->courierReward;
    }

    /**
     * @return User
     */
    public function getCourier()
    {
        return $this->courier;
    }
}