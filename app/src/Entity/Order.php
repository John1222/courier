<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="`orders`")
 **/
class Order
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @var int
     */
    protected $id;
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @var User
     */
    protected $client;
    /**
     * @ORM\Column(type="boolean", name="is_active")
     */
    protected $isActive = true;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @return bool
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param User $client
     */
    public function setClient( User $client )
    {
        $this->client = $client;
    }

    /**
     * @param bool $isActive
     */
    public function setActive( bool $isActive ): void
    {
        $this->isActive = $isActive;
    }
}