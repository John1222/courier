<?php
namespace App\DataFixtures;

use App\Entity\Delivery;
use App\Entity\Order;
use App\Entity\Shipment;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Tools\SchemaTool;

class AppFixtures extends Fixture
{
    public function load( ObjectManager $manager )
    {
        $schemaTool = new SchemaTool( $manager );
        $schemaTool->dropDatabase();
        $schemaTool->createSchema( $manager->getMetadataFactory()->getAllMetadata() );

        $client = new User;
        $client->setName( 'Андрей Баблодавов' );
        $client->setClient( true );
        $client->increaseBalance( 1000 );
        $manager->persist( $client );

        $order = new Order;
        $order->setClient( $client );
        $manager->persist( $order );

        $orderClosed = new Order;
        $orderClosed->setClient( $client );
        $orderClosed->setActive( false );
        $manager->persist( $orderClosed );
        
        $courier = new User;
        $courier->setName( 'Студент Курьеров' );
        $manager->persist( $courier );

        $shipment = new Shipment;
        $shipment->setLocation( 'Москва, улица Складских помещений, строение 8' );
        $shipment->setGood( 'Водка x100' );
        $manager->persist( $shipment );

        $delivery = new Delivery;
        $delivery->setLocation( 'Москва, улица Алконавтов, дом 25' );
        $delivery->setOrder( $order );
        $delivery->setShipment( $shipment );
        $delivery->setCourier( $courier );
        $delivery->setCourierReward( 500 );
        $manager->persist( $delivery );

        $shipmentWithWrongLocation = new Shipment;
        $shipmentWithWrongLocation->setLocation( 'Москва, улица Алконавтов, дом 25' );
        $shipmentWithWrongLocation->setGood( 'Водка x100' );
        $manager->persist( $shipmentWithWrongLocation );

        $deliveryWithWrongLocation = new Delivery;
        $deliveryWithWrongLocation->setLocation( 'Москва, улица Алконавтов, дом 25' );
        $deliveryWithWrongLocation->setOrder( $order );
        $deliveryWithWrongLocation->setShipment( $shipmentWithWrongLocation );
        $deliveryWithWrongLocation->setCourier( $courier );
        $deliveryWithWrongLocation->setCourierReward( 500 );
        $manager->persist( $deliveryWithWrongLocation );
        
        $manager->flush();
    }
}
