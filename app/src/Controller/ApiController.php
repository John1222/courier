<?php
namespace App\Controller;

use App\Entity\User;
use App\Service\Bank;
use App\Service\BankException;
use App\Service\DeliveryDispatcher;
use App\Service\DeliveryDispatcherException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

// TODO добавить 404 page в симфоню
class ApiController extends AbstractController
{
    /**
     * @Route("/api/delivery_execute/{id}")
     */
    public function deliveryExecute( $id, DeliveryDispatcher $deliveryDispatcher, Bank $bank )
    {
        try {
            $delivery = $deliveryDispatcher->executeDelivery( $id );
            $bank->transferMoney(
                $delivery->getOrder()->getClient(),
                $delivery->getCourier(),
                $delivery->getCourierReward()
            );
            return new Response( sprintf(
                "Delivery completed! Shipment delivered from location '%s' to '%s'. Courier %s rewarded for %s$ by %s",
                $delivery->getShipment()->getLocation(),
                $delivery->getLocation(),
                $delivery->getCourier()->getName(),
                $delivery->getCourierReward(),
                $delivery->getOrder()->getClient()->getName()
            ) );
        }
        catch( DeliveryDispatcherException $e ) {
            return new Response( $e->getMessage() );
        }
    }

    /**
     * @Route("/api/money_transfer/")
     */
    public function moneyTransfer( Request $request, Bank $bank )
    {
        /** @var User $sender */
        $sender = $this->getDoctrine()->getRepository( User::class )->find( $request->get( 'sender' ) );
        /** @var User $recipient */
        $recipient = $this->getDoctrine()->getRepository( User::class )->find( $request->get( 'recipient' ) );

        try {
            $amount = $request->get( 'amount' );
            $bank->transferMoney( $sender, $recipient, $amount );
            return new Response( sprintf(
                'Transfer completed! Sender %s sent %s %s$',
                $sender->getName(), $recipient->getName(), $amount
            ) );
        }
        catch( BankException $e ) {
            return new Response( $e->getMessage() );
        }
    }
}