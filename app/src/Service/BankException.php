<?php
namespace App\Service;

use Exception;

class BankException extends Exception
{
    public function __construct( $message = '' )
    {
        parent::__construct( $message );
    }
}