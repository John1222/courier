<?php
namespace App\Service;

use App\Entity\Transaction;
use App\Entity\User;
use DateTime;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class Bank
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct( EntityManagerInterface $em )
    {
        $this->em = $em;
    }

    /**
     * @param User $sender
     * @param User $recipient
     * @param int $amount
     * @throws BankException
     */
    public function transferMoney( User $sender, User $recipient, $amount )
    {
        $this->em->transactional( function() use ( $sender, $recipient, $amount ) {
            $this->em->lock( $sender, LockMode::PESSIMISTIC_WRITE );
            $this->em->lock( $recipient, LockMode::PESSIMISTIC_WRITE );

            $this->em->refresh( $sender );
            $this->em->refresh( $recipient );

            if( $sender->getBalance() - $amount < 0 ) {
                throw new BankException(
                    'Sender has insufficient funds'
                );
            }

            $sender->decreaseBalance( $amount );
            $recipient->increaseBalance( $amount );

            $transaction = new Transaction;
            $transaction->setAmount( $amount );
            $transaction->setSender( $sender );
            $transaction->setRecipient( $recipient );
            $transaction->setDate( new DateTime( 'now' ) );

            $this->em->persist( $sender );
            $this->em->persist( $recipient );
            $this->em->persist( $transaction );
        } );
    }
}