<?php
namespace App\Service;

use App\Entity\Delivery;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class DeliveryDispatcher
{
    /**
     * @var EntityManager
     */
    private $em;
    
    public function __construct( EntityManagerInterface $em )
    {
        $this->em = $em;
    }

    /**
     * @param int $id
     * @return Delivery
     * @throws DeliveryDispatcherException
     */
    public function executeDelivery( $id )
    {
        $delivery = null;
        $this->em->transactional( function() use( $id, &$delivery ) {
            /** @var Delivery $delivery */
            $delivery = $this->em->getRepository( Delivery::class )->find( $id, LockMode::PESSIMISTIC_WRITE );
            $shipment = $delivery->getShipment();

            if( ! $delivery->getIsActive() ) {
                throw new DeliveryDispatcherException( 'Delivery closed' );
            }
            elseif( $delivery->getLocation() == $shipment->getLocation() ) {
                throw new DeliveryDispatcherException( "Shipment already in location '{$delivery->getLocation()}'" );
            }
            elseif( $delivery->getOrder()->getClient()->getBalance() < $delivery->getCourierReward() ) {
                throw new DeliveryDispatcherException( 'Client have not enough funds for courier reward' );
            }

            $delivery->setActive( false );
            $shipment->setLocation( $delivery->getLocation() );

            $this->em->persist( $delivery );
            $this->em->persist( $shipment );
        });
        return $delivery;
    }
}