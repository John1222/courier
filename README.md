# Развертывание

Развертывание тестировалось на 18.04.3 (LTS) x64

Выполнить в консоли:
```sh
cd /home/ && sudo curl https://bitbucket.org/brocoder1/couriertest/downloads/deploy.sh -L -o ./deploy.sh && sudo chmod +x ./deploy.sh && sudo ./deploy.sh
```

В конце запустит тесты. Если все прошло хорошо, то все они будут пройдены.

На 80-м порту будет поднят nginx с Symfony и базой Postgres с тестовыми данными. Базу не стал закрывать, чтобы проще было подключиться и потыкать:
```
postgresql://postgres:123456@[ип серва]:5432/database
```

# API

Осуществить доставку по созданной заявке:

```bash
GET /api/delivery_execute/[delivery_id]
```

Перевести деньги между пользователями:

```bash
GET /api/money_transfer/?sender=[id]&recipient=[id]&amount=[amount]
```